package com.kadirsahin.restaurantapp.model.error;

import android.support.annotation.NonNull;

import java.sql.Timestamp;

public class ResponseError {

    private Timestamp timestamp;

    private Integer status;

    private String error;

    private String message;

    private FormContext context;

    public ResponseError() {
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FormContext getContext() {
        return context;
    }

    public void setContext(FormContext context) {
        this.context = context;
    }

    @NonNull
    @Override
    public String toString() {
        return "ResponseError{" +
                "timestamp=" + timestamp +
                ", status=" + status +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
