package com.kadirsahin.restaurantapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kadirsahin.restaurantapp.service.retrofit.annotation.JsonKey;

import java.util.Date;

@JsonKey(value = "user", firstKey = true)
public class User {

    private Long id;

    private String email;

    private String name;

    private String address;

    private String identity;

    private String username;

    private String gender;

    private boolean permittedUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date birthdate;

    private boolean cardStoreAgreement;

    private boolean hasPassword;

    private boolean verified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isCardStoreAgreement() {
        return cardStoreAgreement;
    }

    public void setCardStoreAgreement(boolean cardStoreAgreement) {
        this.cardStoreAgreement = cardStoreAgreement;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public boolean isPermittedUser() {
        return permittedUser;
    }

    public void setPermittedUser(boolean permittedUser) {
        this.permittedUser = permittedUser;
    }
}
