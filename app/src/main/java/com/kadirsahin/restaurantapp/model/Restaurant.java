package com.kadirsahin.restaurantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.kadirsahin.restaurantapp.service.retrofit.annotation.JsonKey;

import java.util.ArrayList;

/**
 * Created by enginsahin on 17.07.2016.
 */
@JsonKey(firstKey = true, value = {"restaurant", "restaurants"})
public class Restaurant implements Parcelable {

    private Long id;

    private String name;

    private String city;

    private Image image;

    private String address;

    private boolean publicProducts;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    ArrayList<Category> categories;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isPublicProducts() {
        return publicProducts;
    }

    public void setPublicProducts(boolean publicProducts) {
        this.publicProducts = publicProducts;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.city);
        dest.writeParcelable(this.image, flags);
        dest.writeString(this.address);
        dest.writeTypedList(this.categories);
    }

    public Restaurant() {
    }

    protected Restaurant(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.city = in.readString();
        this.image = in.readParcelable(Image.class.getClassLoader());
        this.address = in.readString();
        this.categories = in.createTypedArrayList(Category.CREATOR);
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel source) {
            return new Restaurant(source);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };
}
