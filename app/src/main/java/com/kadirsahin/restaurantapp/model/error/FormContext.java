package com.kadirsahin.restaurantapp.model.error;

import java.util.List;

public class FormContext {

    private List<Error> errors;

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
