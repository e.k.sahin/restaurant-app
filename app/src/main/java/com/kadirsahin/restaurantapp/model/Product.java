package com.kadirsahin.restaurantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;


public class Product implements Parcelable {

    private Long id;

    private String name;

    private String information;

    private BigDecimal price;

    private String formattedPrice;

    private Long time;

    private String image;

    private String imageBig;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getFormattedPrice() {
        return formattedPrice;
    }

    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageBig() {
        return imageBig;
    }

    public void setImageBig(String imageBig) {
        this.imageBig = imageBig;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.information);
        dest.writeSerializable(this.price);
        dest.writeString(this.formattedPrice);
        dest.writeValue(this.time);
        dest.writeString(this.image);
        dest.writeString(this.imageBig);
    }

    public Product() {
    }

    protected Product(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.information = in.readString();
        this.price = (BigDecimal) in.readSerializable();
        this.formattedPrice = in.readString();
        this.time = (Long) in.readValue(Long.class.getClassLoader());
        this.image = in.readString();
        this.imageBig = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
