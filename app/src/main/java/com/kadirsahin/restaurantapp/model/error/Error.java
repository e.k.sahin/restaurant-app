package com.kadirsahin.restaurantapp.model.error;

public class Error {

    private String message;

    private String field;

    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Error{" +
                "message='" + message + '\'' +
                ", field='" + field + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
