package com.kadirsahin.restaurantapp.login;

import com.kadirsahin.restaurantapp.service.retrofit.Response;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApiService {


    @FormUrlEncoded
    @POST(value = "api/login")
    Single<Response> login(
            @Field("username") String email,
            @Field("password") String pass);
}
