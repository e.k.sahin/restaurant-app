package com.kadirsahin.restaurantapp.login;

import com.kadirsahin.restaurantapp.model.Restaurant;
import com.kadirsahin.restaurantapp.model.User;
import com.kadirsahin.restaurantapp.model.error.ResponseError;
import com.kadirsahin.restaurantapp.service.retrofit.Response;

import java.util.List;

/**
 * Created by kadirsahin on 23/04/2018.
 */

public interface LoginContract {

    interface LoginView {

        void serverError(ResponseError error);

        void setLoadingIndicator(boolean show);

        void showToast(String message);

        void success(User user, Restaurant restaurant);
    }

    interface LoginPresenter {

        void login(String email, String password);
    }
}
