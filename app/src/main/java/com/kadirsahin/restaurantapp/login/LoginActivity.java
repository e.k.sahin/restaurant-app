package com.kadirsahin.restaurantapp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kadirsahin.restaurantapp.R;
import com.kadirsahin.restaurantapp.category.list.CategoryActivity;
import com.kadirsahin.restaurantapp.custom.LoaderDialog;
import com.kadirsahin.restaurantapp.model.Category;
import com.kadirsahin.restaurantapp.model.Restaurant;
import com.kadirsahin.restaurantapp.model.User;
import com.kadirsahin.restaurantapp.model.error.Error;
import com.kadirsahin.restaurantapp.model.error.ResponseError;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class LoginActivity extends AppCompatActivity implements LoginContract.LoginView, Validator.ValidationListener {

    private LoginContract.LoginPresenter mLoginPresenter;

    private Validator mValidator;

    @BindView(android.R.id.content)
    View mRoot;

    @Email()
    @NotEmpty()
    @BindView(R.id.email)
    EditText mEmailView;

    @NotEmpty
    @Password()
    @BindView(R.id.password)
    EditText mPasswordView;
    private LoaderDialog mLoaderDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginPresenter = new LoginPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        this.mLoaderDialog = new LoaderDialog();

    }

    @OnClick(R.id.submit)
    public void submit(Button button) {
        mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        mLoginPresenter.login(mEmailView.getText().toString(), mPasswordView.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void serverError(ResponseError err) {
        Timber.d("ServerError");
        List<Error> errors = err.getContext().getErrors();

        for (Error error : errors) {
            switch (error.getField()) {
                case "email":
                    mEmailView.setError(error.getMessage());
                    break;
                case "password":
                    mPasswordView.setError(error.getMessage());
                    break;
            }
        }
    }

    @Override
    public void success(User user, Restaurant restaurant) {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(CategoryActivity.RESTAURANT_ARG, restaurant);
        startActivity(intent);
    }

    @Override
    public void setLoadingIndicator(boolean show) {
        if (show && !mLoaderDialog.isAdded())
            mLoaderDialog.show(getFragmentManager(), null);
        else if (!show && mLoaderDialog.isAdded())
            mLoaderDialog.dismiss();
    }

    @Override
    public void showToast(String message) {
        Snackbar.make(this.mRoot, message, Snackbar.LENGTH_LONG).show();
    }
}

