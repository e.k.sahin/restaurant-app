package com.kadirsahin.restaurantapp.login;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.kadirsahin.restaurantapp.category.list.CategoryApiService;
import com.kadirsahin.restaurantapp.model.Restaurant;
import com.kadirsahin.restaurantapp.model.User;
import com.kadirsahin.restaurantapp.model.error.ResponseError;
import com.kadirsahin.restaurantapp.mvp.BasePresenter;
import com.kadirsahin.restaurantapp.service.TokenUtils;
import com.kadirsahin.restaurantapp.service.retrofit.ApiClient;
import com.kadirsahin.restaurantapp.service.retrofit.Response;
import com.kadirsahin.restaurantapp.service.retrofit.ResponseUtils;

import java.net.HttpURLConnection;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class LoginPresenter extends BasePresenter<LoginContract.LoginView>
        implements LoginContract.LoginPresenter {

    private final LoginApiService mLoginService;

    private final BehaviorRelay<RequestState> requestStateObserver
            = BehaviorRelay.createDefault(RequestState.IDLE);

    private final CategoryApiService mProductService;

    protected LoginPresenter(LoginContract.LoginView view) {
        super(view);
        this.mLoginService = ApiClient.getInstance().getApiImplementer(LoginApiService.class);
        this.mProductService = ApiClient.getInstance().getApiImplementer(CategoryApiService.class);
        observeRequestState();
    }

    @Override
    public void login(String email, String password) {
        this.mLoginService.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> publishRequestState(RequestState.LOADING))
                .doOnError(t -> publishRequestState(RequestState.ERROR))
                .subscribe(
                        this::handleLoginResponse,
                        this::handleServerError);
    }

    private void handleLoginResponse(Response userResponse) {
        saveToken(userResponse);
        this.mProductService.getRestaurantMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> publishRequestState(RequestState.LOADING))
                .doOnSuccess(s -> publishRequestState(RequestState.COMPLETE))
                .doOnError(s -> publishRequestState(RequestState.ERROR))
                .subscribe(
                        response -> this.handleRestaurantData(userResponse, response),
                        this::handleServerError);

    }

    private void handleRestaurantData(Response userResponse, Response restaurantResponse) {
        view.success(
                userResponse.get(User.class),
                restaurantResponse.get(Restaurant.class)
        );
    }

    private void saveToken(Response response) {
        String accessToken = response.get("token", String.class);
        TokenUtils.saveToken(accessToken);
    }

    private void publishRequestState(RequestState requestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(requestStateObserver));
    }

    private void observeRequestState() {
        requestStateObserver.subscribe(requestState -> {
            switch (requestState) {
                case IDLE:
                    break;
                case LOADING:
                    view.setLoadingIndicator(true);
                    break;
                case COMPLETE:
                    view.setLoadingIndicator(false);
                    break;
                case ERROR:
                    view.setLoadingIndicator(false);
                    break;
            }
        }, Timber::e);
    }

    private void handleServerError(Throwable e) {
        HttpException httpException = (HttpException) e;
        ResponseError error = ResponseUtils.responseFormError(httpException.response().errorBody());
        switch (httpException.code()) {
            case HttpURLConnection.HTTP_BAD_REQUEST:
                view.serverError(error);
                break;
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                view.showToast(error.getMessage());
                break;

        }

    }

}
