package com.kadirsahin.restaurantapp.service.retrofit;

import com.kadirsahin.restaurantapp.service.TokenUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kadirsahin on 23/04/2018.
 */

class TokenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        builder.addHeader("Accept", "application/json;versions=1");
        if (TokenUtils.hasToken()) {
            builder.addHeader("X-Auth-Token", TokenUtils.getToken());
        }
        return chain.proceed(builder.build());
    }
}
