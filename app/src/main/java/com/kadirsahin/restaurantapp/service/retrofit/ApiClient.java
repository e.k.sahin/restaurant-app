package com.kadirsahin.restaurantapp.service.retrofit;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.kadirsahin.restaurantapp.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by kadirsahin on 23/04/2018.
 */
public class ApiClient {


    private static final ApiClient ourInstance = new ApiClient();
    private final Retrofit retrofit;
    private final OkHttpClient client;

    public static ApiClient getInstance() {
        return ourInstance;
    }

    private ApiClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(new TokenInterceptor());

        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(loggingInterceptor);
        }

        client = okHttpBuilder.build();

        JacksonInstance instance = JacksonInstance.getInstance();

        retrofit = new Retrofit.Builder().baseUrl(BuildConfig.API_URL)
                .addConverterFactory(JacksonConverterFactory.create(instance.getObjectMapper()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public OkHttpClient getClient() {
        return client;
    }

    public <I> I getApiImplementer(Class<I> i) {
        return retrofit.create(i);
    }
}
