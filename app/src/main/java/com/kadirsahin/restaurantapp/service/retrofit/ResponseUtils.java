package com.kadirsahin.restaurantapp.service.retrofit;


import android.support.annotation.Nullable;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.kadirsahin.restaurantapp.model.error.ResponseError;
import com.kadirsahin.restaurantapp.service.retrofit.annotation.JsonKey;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;

public class ResponseUtils {

    public static final String TAG = ResponseUtils.class.getCanonicalName();

    public static <T> T get(JsonNode data, Class<T> clazz) {


        JacksonInstance jacksonInstance = JacksonInstance.getInstance();

        JsonKey jsonKey = clazz.getAnnotation(JsonKey.class);
        T o = null;

        if (jsonKey != null) {
            if (jsonKey.firstKey()) {
                // lookup first in key. if key not found it, this time try root json object
                o = findFirstKey(jacksonInstance, data, jsonKey, clazz);
                if (o == null) {
                    try {
                        o = findInData(jacksonInstance, data, clazz);
                    } catch (IllegalArgumentException ignored) {
                    }
                }

            } else {
                // try root json object
                try {
                    o = findInData(jacksonInstance, data, clazz);
                } catch (IllegalArgumentException ignored) {
                }
                if (o == null) {
                    // else
                    o = findFirstKey(jacksonInstance, data, jsonKey, clazz);
                }
            }
        } else {
            try {
                o = findInData(jacksonInstance, data, clazz);
            } catch (IllegalArgumentException ignored) {
            }
        }

        return o;
    }


    public static <T> List<T> getAsList(JsonNode data, Class<T> clazz) {

        JacksonInstance jacksonInstance = JacksonInstance.getInstance();

        JsonKey jsonKey = clazz.getAnnotation(JsonKey.class);
        List<T> o = null;
        if (jsonKey != null) {
            if (jsonKey.firstKey()) {
                // lookup first in key. if key not found it, this time try root json object
                o = findFirstKeyAsList(jacksonInstance, data, jsonKey, clazz);
                if (o == null) {
                    findInDataAsList(jacksonInstance, data, clazz);
                }

            } else {
                // try root json object
                o = findInDataAsList(jacksonInstance, data, clazz);
                if (o == null) {
                    // else
                    o = findFirstKeyAsList(jacksonInstance, data, jsonKey, clazz);
                }
            }
        } else {
            o = findInDataAsList(jacksonInstance, data, clazz);
        }

        return o;
    }

    public static ResponseError responseFormError(ResponseBody responseBody) {
        JacksonInstance instance = JacksonInstance.getInstance();
        try {
            return instance.getObjectMapper()
                    .readValue(responseBody.string(), ResponseError.class);
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
            return null;

        }

    }

    @Nullable
    private static <T> List<T> findFirstKeyAsList(
            JacksonInstance jacksonInstance, JsonNode data, JsonKey jsonKey, Class<T> clazz) {
        for (String key: jsonKey.value()) {
            JsonNode jsonNode = data.get(key);
            try {
                if (jsonNode != null) {
                    return jacksonInstance.getObjectMapper().convertValue(
                            jsonNode, TypeFactory.defaultInstance()
                                    .constructCollectionType(ArrayList.class, clazz));
                }
            } catch (IllegalArgumentException e) {
                Log.w(TAG, e.getLocalizedMessage().concat(" ").concat(clazz.getCanonicalName()));
            }
        }


        return null;
    }

    @Nullable
    private static <T> List<T> findInDataAsList(
            JacksonInstance jacksonInstance, JsonNode data, Class<T> clazz) {
        try {

            return jacksonInstance.getObjectMapper().convertValue(
                    data, TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, clazz));
        } catch (IllegalArgumentException e) {
            Log.w(TAG, e.getLocalizedMessage().concat(" ").concat(clazz.getCanonicalName()));
            return null;
        }

    }


    @Nullable
    private static <T> T findFirstKey(
            JacksonInstance jacksonInstance, JsonNode data, JsonKey jsonKey, Class<T> clazz) {
        for (String s : jsonKey.value()) {
            JsonNode jsonNode = data.get(s);
            try {
                if (jsonNode != null) {
                    return jacksonInstance.getObjectMapper().convertValue(
                            jsonNode, clazz);
                }
            } catch (IllegalArgumentException e) {
                Log.w(TAG, e.getLocalizedMessage().concat(" ").concat(clazz.getCanonicalName()));
            }
        }

        return null;
    }

    @Nullable
    private static <T> T findInData(
            JacksonInstance jacksonInstance, JsonNode data, Class<T> clazz) throws IllegalArgumentException{
        return jacksonInstance.getObjectMapper().convertValue(
                data, clazz);

    }
}
