package com.kadirsahin.restaurantapp.service.retrofit.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(ElementType.TYPE)
@Retention(RUNTIME)
public @interface JsonKey {

    /**
     * 'data' key'inin 1 seviye altında bulunan key
     * @return
     */
    String[] value();

    /**
     * Eger true ise ${@link JsonKey#value()} key'i json data'sında once o key'i arar ve bulamazsa
     * json nesnesinin 'data' key'inde arar. false ise ilk once 'data' key'ini parse eder.
     * @return
     */
    boolean firstKey() default false;
}
