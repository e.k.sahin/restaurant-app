package com.kadirsahin.restaurantapp.service.retrofit;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonInstance {
    private static JacksonInstance ourInstance = new JacksonInstance();

    public static JacksonInstance getInstance() {
        return ourInstance;
    }

    private ObjectMapper objectMapper;

    private JacksonInstance() {
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        jsonFactory.configure(JsonParser.Feature.IGNORE_UNDEFINED, true);
        this.objectMapper = new ObjectMapper(jsonFactory);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
