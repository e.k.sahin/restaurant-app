package com.kadirsahin.restaurantapp.service.retrofit;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class Response {

    private boolean status;

    private String message;

    private JsonNode context;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonNode getContext() {
        return context;
    }

    public void setContext(JsonNode context) {
        this.context = context;
    }

    public static class Meta {

        Integer code;

        boolean status;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }
    }

    public <T> T get(Class<T> clazz) {
        return ResponseUtils.get(context, clazz);
    }

    public <T> T get(String key, Class<T> clazz) {
        return ResponseUtils.get(context.get(key), clazz);
    }

    public <T> List<T> getAsList(Class<T> clazz) {
        return ResponseUtils.getAsList(context, clazz);
    }

    public <T> List<T> getAsList(String key, Class<T> clazz) {
        return ResponseUtils.getAsList(context.get(key), clazz);
    }

}
