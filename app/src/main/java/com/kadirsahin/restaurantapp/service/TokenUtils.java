package com.kadirsahin.restaurantapp.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.kadirsahin.restaurantapp.App;
import com.kadirsahin.restaurantapp.BuildConfig;

public class TokenUtils {

    private static final String TOKEN_KEY = "token";

    public static void saveToken(String token) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    public static String getToken() {
        return getSharedPreferences().getString(TOKEN_KEY, null);
    }

    public static boolean hasToken() {
        return getToken() != null && !getToken().trim().isEmpty();
    }

    private static SharedPreferences getSharedPreferences() {
        return App.getAppContext()
                .getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }
}
