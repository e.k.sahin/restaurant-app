package com.kadirsahin.restaurantapp.category.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kadirsahin.restaurantapp.R;
import com.kadirsahin.restaurantapp.model.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private List<Category> mCategory;

    private Events.OnClickEvent onClickEvent;

    public CategoryListAdapter(List<Category> mCategory) {
        this.mCategory = mCategory;
    }


    @NonNull
    @Override
    public CategoryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_category_list_item, parent, false);
        return new CategoryListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListAdapter.ViewHolder holder, int position) {
        Category category = mCategory.get(position);

        Picasso.get().load(category.getImage().getPath())
                .into(holder.mImage);
        holder.mTitle.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        return mCategory.size();
    }

    public void setOnClickEvent(Events.OnClickEvent onClickEvent) {
        this.onClickEvent = onClickEvent;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView mImage;

        @BindView(R.id.title)
        TextView mTitle;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.container)
        public void onClick(View view) {
            if (onClickEvent != null) onClickEvent.onClick(view, getAdapterPosition());
        }
    }


}
