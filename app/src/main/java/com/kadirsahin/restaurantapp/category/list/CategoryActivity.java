package com.kadirsahin.restaurantapp.category.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.kadirsahin.restaurantapp.R;
import com.kadirsahin.restaurantapp.category.detail.CategoryDetailActivity;
import com.kadirsahin.restaurantapp.model.Category;
import com.kadirsahin.restaurantapp.model.Restaurant;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class CategoryActivity extends AppCompatActivity implements CategoryContract.CategoryView, Events.OnClickEvent {

    public static final String RESTAURANT_ARG = "restaurant";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.listView)
    RecyclerView mListView;

    private CategoryPresenter mCategoryPresenter;
    private Restaurant mRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        mCategoryPresenter = new CategoryPresenter(this);
        this.mRestaurant = getIntent().getParcelableExtra(RESTAURANT_ARG);

        initToolbar();

        buildView();
    }

    private void buildView() {
        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(mRestaurant.getCategories());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mListView.setLayoutManager(mLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(categoryListAdapter);
        categoryListAdapter.setOnClickEvent(this);

    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int adapterPosition) {
        Intent intent = new Intent(this, CategoryDetailActivity.class);
        Category category = mRestaurant.getCategories().get(adapterPosition);
        intent.putExtra(CategoryDetailActivity.CATEGORY_ARG, category);

        Pair<View, String> imagePair = Pair.create(view.findViewById(R.id.imageView), "categoryImage");
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this, imagePair);
        startActivity(intent, optionsCompat.toBundle());
    }
}
