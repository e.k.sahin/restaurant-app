package com.kadirsahin.restaurantapp.category.detail;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.kadirsahin.restaurantapp.R;
import com.kadirsahin.restaurantapp.category.list.CategoryListAdapter;
import com.kadirsahin.restaurantapp.model.Category;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryDetailActivity extends AppCompatActivity {

    public static final String CATEGORY_ARG = "category";


    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.categoryImage)
    ImageView mCategoryImage;

    @BindView(R.id.listView)
    RecyclerView mListView;

    Category mCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        ButterKnife.bind(this);
        mCategory = getIntent().getParcelableExtra(CATEGORY_ARG);

        Picasso.get().load(mCategory.getImage().getPath()).into(mCategoryImage);



        initToolbar();

        buildView();
    }


    private void buildView() {
        ProductListViewAdapter productListViewAdapter = new ProductListViewAdapter(mCategory.getProducts());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        mListView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mListView.setHasFixedSize(true);
        mListView.setLayoutManager(mLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(productListViewAdapter);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mCategory.getName());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
