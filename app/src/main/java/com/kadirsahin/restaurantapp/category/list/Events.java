package com.kadirsahin.restaurantapp.category.list;

import android.view.View;

public interface Events {

    interface OnClickEvent {
        void onClick(View view, int adapterPosition);
    }
}
