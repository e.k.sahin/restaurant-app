package com.kadirsahin.restaurantapp.category.list;

import com.kadirsahin.restaurantapp.mvp.BasePresenter;

public class CategoryPresenter extends BasePresenter<CategoryContract.CategoryView> implements CategoryContract.CategoryPresenter {

    protected CategoryPresenter(CategoryContract.CategoryView view) {
        super(view);
    }
}
