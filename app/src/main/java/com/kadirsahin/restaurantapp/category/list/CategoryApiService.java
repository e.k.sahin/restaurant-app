package com.kadirsahin.restaurantapp.category.list;

import com.kadirsahin.restaurantapp.service.retrofit.Response;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface CategoryApiService {

    @GET("api/restaurant/11/detail")
    Single<Response> getRestaurantMenu();
}
