package com.kadirsahin.restaurantapp;

import android.app.Application;
import android.content.Context;

import timber.log.Timber;

public class App extends Application {

    private static Context appContext;

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Stetho.initializeWithDefaults(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        appContext = this;
    }
}
