package com.kadirsahin.restaurantapp.custom;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.kadirsahin.restaurantapp.R;

public class LoaderDialog extends DialogFragment {


    private LoaderView mLoaderView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = initView();
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    private View initView() {
        View inflate = LayoutInflater.from(getActivity()).inflate(R.layout.loader_dialog, null, false);
        this.mLoaderView = inflate.findViewById(R.id.loader);
        mLoaderView.startAnimation();
        return inflate;
    }

}
