package com.kadirsahin.restaurantapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.kadirsahin.restaurantapp.R;

public class LoaderView extends android.support.v7.widget.AppCompatImageView {

    private Animation animation;

    public LoaderView(Context context) {
        super(context);
    }

    public LoaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LoaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }


    public void startAnimation() {
        this.animation = AnimationUtils.loadAnimation(getContext(), R.anim.loader_animation);
        startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.w("LoaderView", "On animation end");
                Drawable drawable = getDrawable();
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).start();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);


    }

}
