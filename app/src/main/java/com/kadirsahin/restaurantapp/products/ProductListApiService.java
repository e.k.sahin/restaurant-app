package com.kadirsahin.restaurantapp.products;

import com.kadirsahin.restaurantapp.service.retrofit.Response;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ProductListApiService {

    @GET("api/restaurant/11/detail")
    Single<Response> getRestaurantMenu();
}
