package com.kadirsahin.restaurantapp.products;

import com.kadirsahin.restaurantapp.mvp.BasePresenter;

public class ProductListPresenter extends BasePresenter<ProductListContract.ProductsView> implements ProductListContract.ProductsPresenter {

    protected ProductListPresenter(ProductListContract.ProductsView view) {
        super(view);
    }
}
